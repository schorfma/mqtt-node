"""MQTT Node Sensors Registry"""
import catalogue

SENSORS = catalogue.create("mqtt_node", "sensors")

try:
    from .open_weather_api import OpenWeatherAPISensor
    SENSORS.register(name="weather", func=OpenWeatherAPISensor())
except Exception:
    pass

try:
    from .bmp280_sensor import BMP280Sensor
    SENSORS.register(name="bmp280", func=BMP280Sensor())
    SENSORS.register(name="temperature",
                     func=SENSORS.get("bmp280").temperature)
    SENSORS.register(name="pressure", func=SENSORS.get("bmp280").pressure)
except Exception:
    pass

try:
    from .dht22_sensor import DHT22Sensor
    SENSORS.register(name="dht22", func=DHT22Sensor())
    SENSORS.register(name="temperature", func=SENSORS.get("dht22").temperature)
    SENSORS.register(name="humidity", func=SENSORS.get("dht22").humidity)
except Exception:
    pass
