"""DHT22 sensor (temperature, humidity)"""
from typing import Dict
from typing import Optional

from adafruit_dht import DHT22
from board import D16 as PIN
from pydantic import BaseSettings

DHT22_SENSOR = DHT22(PIN)


class DHT22Sensor(BaseSettings):
    """DHT22 sensor wrapper."""

    cached_dht22_sensor: Optional[DHT22]

    @property
    def sensor(self) -> DHT22:
        """The DHT22 sensor instance."""
        if not self.cached_dht22_sensor:
            self.cached_dht22_sensor = DHT22_SENSOR

        return self.cached_dht22_sensor

    def temperature(self) -> float:
        """Returns current temperature sensor readings in degrees Celsius."""
        return self.sensor.temperature

    def humidity(self) -> float:
        """Returns current humidity sensor readings in percent."""
        return self.sensor.humidity

    def __call__(self) -> Dict:
        """Return both temperature and humidity sensor readings."""
        return {
            "temperature": self.temperature(),
            "humidity": self.humidity()
        }
