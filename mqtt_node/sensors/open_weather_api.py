"""Module for sensor based on OpenWeatherMap API."""
from typing import Dict
from typing import Literal
from typing import Optional
from typing import Text

import requests
from pydantic import BaseSettings
from pydantic.class_validators import (  # pylint: disable=no-name-in-module
    root_validator
)
from pydantic.networks import AnyHttpUrl  # pylint: disable=no-name-in-module


class OpenWeatherAPISensor(BaseSettings):
    """Sensor for outside temperature, pressure, humidity and more using
    the OpenWeatherMap API."""

    open_weather_api_key: Text
    geo_coding_api_endpoint: AnyHttpUrl = "http://api.openweathermap.org/geo/1.0/direct"
    weather_api_endpoint: AnyHttpUrl = "http://api.openweathermap.org/data/2.5/weather"

    city_name: Optional[Text]
    state_code: Optional[Text]  # Only for US
    country_code: Optional[Text]  # ISO 3166 country codes, e.g. "DE"

    latitude: Optional[float]
    longitude: Optional[float]

    open_weather_api_units: Literal[
        "standard",
        "metric",
        "imperial"
    ] = "metric"

    @classmethod
    def geo_coding_location_query(
            cls,
            *location_parts: Optional[Text],
            join_chars: Text = ","
    ) -> Text:
        """Joins geo location parts to format for geo coding location query."""
        return join_chars.join(
            [
                location_part
                for location_part in location_parts
                if location_part
            ]
        )

    @root_validator()
    def determine_geo_location_coordinates(  # pylint: disable=no-self-argument
            cls,
            values: Dict
    ) -> Dict:
        """Determines latitude and longitude from the geo location."""
        if not values.get("latitude") or not values.get("longitude"):
            geo_coding_location_query: Text = cls.geo_coding_location_query(
                values.get("city_name"),
                values.get("state_code"),
                values.get("country_code")
            )

            response: requests.Response = requests.get(
                values["geo_coding_api_endpoint"],
                params={
                    "q": geo_coding_location_query,
                    "appid": values["open_weather_api_key"]
                }
            )

            location: Dict
            location, *_ = response.json()

            values["latitude"] = location["lat"]
            values["longitude"] = location["lon"]

        return values

    def weather(self) -> Dict:
        """Composite weather information by API for location."""
        response: requests.Response = requests.get(
            self.weather_api_endpoint,
            params={
                "lat": self.latitude,
                "lon": self.longitude,
                "appid": self.open_weather_api_key,
                "units": self.open_weather_api_units
            }
        )

        values: Dict = response.json()

        return {
            "location": self.geo_coding_location_query(
                self.city_name,
                self.state_code,
                self.country_code,
                join_chars=", "
            ),
            "latitude": values["coord"]["lat"],
            "longitude": values["coord"]["lon"],
            "weather": values["weather"][0]["main"],
            "temperature": values["main"]["temp"],
            "pressure": values["main"]["pressure"],
            "humidity": values["main"]["humidity"],
            "visibility": values["visibility"],
            "wind_speed": values["wind"]["speed"]
        }

    def __call__(self) -> Dict:
        """Weather information by API."""
        return self.weather()
