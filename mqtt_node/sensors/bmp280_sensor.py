"""BMP280 sensor (temperature, pressure)"""
from typing import Dict
from typing import Literal
from typing import Optional

from bmp280 import BMP280
from pydantic import BaseSettings

from mqtt_node.sensors.open_weather_api import OpenWeatherAPISensor

try:
    from smbus2 import SMBus
except ImportError:
    from smbus import SMBus


BUS = SMBus(1)

BMP280_SENSOR = BMP280(
    i2c_dev=BUS
)


class BMP280Sensor(BaseSettings):
    """Wrapper for BMP280 Sensor."""

    # BMP280 Setup arguments
    bmp280_sensor_mode: Literal["normal", "forced", "sleep"] = "normal"
    bmp280_temperature_oversampling: int = 16
    bmp280_pressure_oversampling: int = 16
    bmp280_temperature_standby: int = 500

    # BMP280 Sensor
    cached_bmp280_sensor: Optional[BMP280]

    # Altitude measurement attributes
    pressure_at_sea_level: float = 1013.25
    outside: bool = False
    cached_weather_api_sensor: Optional[OpenWeatherAPISensor]

    @property
    def sensor(self) -> BMP280:
        """BMP280 sensor for temperature and pressure."""
        if not self.cached_bmp280_sensor:
            self.cached_bmp280_sensor = BMP280_SENSOR
            self.cached_bmp280_sensor.setup(
                mode=self.bmp280_sensor_mode,
                temperature_oversampling=self.bmp280_temperature_oversampling,
                pressure_oversampling=self.bmp280_pressure_oversampling,
                temperature_standby=self.bmp280_temperature_standby
            )

        return self.cached_bmp280_sensor

    @property
    def outside_temperature(self) -> float:
        """Outside temperature based on weather API when sensor inside."""
        if self.outside:
            return self.temperature()

        if not self.cached_weather_api_sensor:
            self.cached_weather_api_sensor = OpenWeatherAPISensor()

        return self.cached_weather_api_sensor.weather()["temperature"]

    def temperature(self) -> float:
        """Current temperature measured by sensor in degrees Celsius."""
        return self.sensor.get_temperature()

    def pressure(self) -> float:
        """Current pressure measured by sensor in hPa."""
        return self.sensor.get_pressure()

    def altitude(self) -> float:
        """Calculates the altitude based on pressure and temperature."""
        return self.sensor.get_altitude(
            qnh=self.pressure_at_sea_level,
            manual_temperature=self.outside_temperature
        )

    def __call__(self) -> Dict:
        """Both temperature and pressure measured by the sensor."""
        return {
            "temperature": self.temperature(),
            "pressure": self.pressure()
        }
