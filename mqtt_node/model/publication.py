"""Publication of MQTT messages."""
from datetime import datetime
from typing import Callable
from typing import Dict
from typing import Optional
from typing import Text
from typing import Union

import paho.mqtt.client as mqtt
import srsly
from rich import print  # pylint: disable=redefined-builtin

from ..settings import SETTINGS
from .action import MqttTopicAction


class Publication(MqttTopicAction):
    """Action for publishing messages to an MQTT topic."""

    retain_last_message: bool = False

    payload_function: Optional[Callable]

    @property
    def payload(self) -> Optional[
            Union[Text, bytes, int, float]
    ]:
        """Property which takes the payload and returns it as modified text."""

        if not self.payload_function:
            raise NotImplementedError

        try:
            payload = self.payload_function()
        except Exception as payload_exception:
            payload = None
            print(f"[red]{payload_exception}[/]")

        if isinstance(payload, Dict):
            if not "datetime" in payload:
                payload["datetime"] = datetime.now().isoformat()

            payload = srsly.json_dumps(payload, indent=4)

        return payload

    def publish(self) -> Optional[Text]:
        """Publishes the payload to its topic and saves it locally."""

        payload = self.payload

        if payload:
            response: mqtt.MQTTMessageInfo = self.client.publish(
                topic=self.topic,
                payload=payload,
                qos=self.quality_of_service_level,
                retain=self.retain_last_message,
                properties=self.properties
            )

            try:
                response.wait_for_publish(timeout=SETTINGS.publication_timeout)
            except (ValueError, RuntimeError) as publication_error:
                print(f"[red]{publication_error}[/]")

            self.save_payload(payload)

        return payload

    def __call__(self) -> Optional[Text]:
        return self.publish()
