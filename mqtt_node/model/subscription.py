"""Subscriptions for subscribing to MQTT topics."""
from datetime import datetime
from typing import Dict
from typing import Optional
from typing import Text
from typing import Union

import paho.mqtt.client as mqtt
from pydantic.class_validators import (  # pylint: disable=no-name-in-module
    validator
)
from rich import print  # pylint: disable=redefined-builtin
from rich.rule import Rule
from typing_extensions import Self

from .action import MqttAction
from .action import MqttTopicAction
from .topic import Topic


class Subscription(MqttTopicAction):
    """Subscription for subscribing to messages of an MQTT topic."""

    options: Optional[mqtt.SubscribeOptions]

    def subscribe(self) -> Self:
        """Subscribes to an MQTT topic and registers the message callback."""
        self.client.subscribe(
            topic=str(self.topic),
            qos=self.quality_of_service_level,
            options=None,
            properties=self.properties
        )

        self.client.message_callback_add(
            str(self.topic),
            self.message_callback
        )

        return self

    def message_callback(
            # pylint: disable=unused-argument
            self,
            client,
            userdata,
            message
    ):
        """Message callback to save received message payloads."""
        print(
            Rule(str(datetime.now()))
        )
        decoded_payload = message.payload.decode()
        decoded_payload_dict = self.save_payload(decoded_payload)
        print(decoded_payload_dict)

    def __call__(self) -> Self:
        return self.subscribe()


class CompositeSubscription(MqttAction):
    """Composite subscription for subscribing to multiple MQTT topics."""

    subscriptions: Dict[Text, Union[Subscription, Topic]]

    @validator("subscriptions")
    def initialize_subscriptions(
            # pylint: disable=no-self-argument, no-self-use
            cls,
            subscriptions: Dict[Text, Union[Subscription, Topic]],
            values: Dict
    ) -> Dict[Text, Subscription]:
        """Initializes child subscriptions."""
        for subscription_key, subscription in subscriptions.items():
            if isinstance(subscription, Topic):
                subscriptions[subscription_key] = Subscription(
                    topic=subscription,
                    quality_of_service_level=values.get(
                        "quality_of_service_level"
                    ),
                    properties=values.get(
                        "properties"
                    ),
                    options=values.get(
                        "options"
                    )
                )

        return subscriptions

    def subscribe(self):
        """Subscribes to multiple MQTT topics."""
        self.client.subscribe(
            [
                (
                    subscription.topic,
                    subscription.quality_of_service_level
                )
                for subscription_key, subscription in self.subscriptions.items()
            ]
        )
