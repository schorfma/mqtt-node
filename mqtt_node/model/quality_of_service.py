"""MQTT quality of service levels."""
from enum import IntEnum


class QualityOfServiceLevel(IntEnum):
    """MQTT quality of service levels enum."""
    QOS_0 = 0
    QOS_1 = 1
    QOS_2 = 2
