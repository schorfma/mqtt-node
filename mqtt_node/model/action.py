"""Base MQTT Actions."""
from datetime import datetime
from pathlib import Path
from typing import Dict
from typing import Optional
from typing import Text
from typing import Tuple
from typing import Union

import paho.mqtt.client as mqtt
import srsly
from pydantic.class_validators import (  # pylint: disable=no-name-in-module
    validator
)

from ..settings import SETTINGS
from .base_model import BaseModel
from .quality_of_service import QualityOfServiceLevel
from .topic import Topic
from .topic import TOPIC_LEVEL_SEPARATOR


class MqttAction(BaseModel):
    """A base MQTT action."""
    # pylint: disable=too-few-public-methods

    quality_of_service_level: QualityOfServiceLevel = (
        QualityOfServiceLevel.QOS_0
    )

    properties: Optional[mqtt.Properties]

    @property
    def client(self) -> mqtt.Client:
        """Property to access the MQTT client from settings."""
        return SETTINGS.client


class MqttTopicAction(MqttAction):
    """MQTT action for a subscribing from or publishing to a certain topic."""

    topic: Topic

    @validator("topic", pre=True)
    def convert_topic(  # pylint: disable=no-self-argument,no-self-use
            cls,
            topic: Union[Topic, Tuple[Text, ...], Text]
    ) -> Topic:
        """Validator to convert different type topics into a Topic object."""

        if not isinstance(topic, Topic):

            if not isinstance(topic, Tuple):
                topic = topic.split(TOPIC_LEVEL_SEPARATOR)

            topic = Topic(topic=topic)

        return topic

    def save_payload(self, payload: Text) -> Dict:
        """Saves the message payload data to file system."""

        date_time: datetime = datetime.now()

        try:
            decoded_payload_dict: Dict = srsly.json_loads(payload)
            assert isinstance(decoded_payload_dict, Dict)
        except Exception:
            decoded_payload_dict: Dict = {
                "datetime": date_time.isoformat(),
                "value": payload
            }

        storage_path: Path = SETTINGS.storage_directory / str(self.topic) / (
            date_time.date().isoformat() + ".jsonl"
        )

        if not storage_path.exists():
            storage_path.parent.mkdir(parents=True, exist_ok=True)
            storage_path.touch()

        srsly.write_jsonl(
            storage_path,
            lines=[decoded_payload_dict],
            append=True,
            append_new_line=False
        )

        return decoded_payload_dict
