"""Base model for other models to inherit from."""
from pydantic import BaseConfig
from pydantic import (  # pylint: disable=no-name-in-module
    BaseModel as PydanticBaseModel
)


class BaseModel(PydanticBaseModel):
    """Pydantic base model."""
    # pylint: disable=too-few-public-methods

    class Config(BaseConfig):
        """Base model Pydantic config."""
        # pylint: disable=too-few-public-methods
        arbitrary_types_allowed: bool = True
