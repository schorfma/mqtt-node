"""Topics for MQTT messages."""
from typing import Text
from typing import Tuple

from pydantic.class_validators import (  # pylint:disable=no-name-in-module
    validator
)
from pydantic.types import NonNegativeInt  # pylint: disable=no-name-in-module

from .base_model import BaseModel


TOPIC_LEVEL_SEPARATOR = "/"


class Topic(BaseModel):
    """MQTT topic representation and parsing."""

    topic: Tuple[Text, ...]

    @validator("topic")
    def normalize_topic(
            # pylint: disable=no-self-argument,no-self-use
            cls,
            topic: Tuple[Text, ...]
    ) -> Tuple[Text, ...]:
        """Normalizes a topic by splitting topic parts on the separator."""
        resulting_topic = []

        for topic_part in topic:
            if TOPIC_LEVEL_SEPARATOR in topic_part:
                resulting_topic.extend(
                    (
                        split_part.strip()
                        for split_part in topic_part.split("/")
                    )
                )
            else:
                resulting_topic.append(topic_part)

        return tuple(resulting_topic)

    def encode(self, encoding: Text) -> Text:
        """Allows use directly with Paho MQTT by duck typing."""
        return str(self).encode(encoding=encoding)

    def __eq__(self, other: "Topic") -> bool:
        for topic_part, other_topic_part in zip(
                self.topic,
                other.topic
        ):
            if topic_part != other_topic_part:
                return False

        return True

    def __str__(self) -> Text:
        return TOPIC_LEVEL_SEPARATOR.join(
            self.topic
        )

    def __len__(self) -> NonNegativeInt:
        return len(
            str(self)
        )
