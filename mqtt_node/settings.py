"""Settings class for MQTT nodes."""
from os.path import getsize as get_file_size
from pathlib import Path
from typing import List
from typing import Optional
from typing import Text
from typing import Union

import paho.mqtt.client as mqtt
import srsly
from dotenv import load_dotenv
from pydantic import BaseConfig
from pydantic import BaseSettings
from pydantic import Extra
from pydantic.class_validators import (  # pylint: disable=no-name-in-module
    validator
)
from pydantic.types import PositiveFloat  # pylint: disable=no-name-in-module
from pydantic.types import PositiveInt  # pylint: disable=no-name-in-module
from rich import print  # pylint: disable=redefined-builtin

load_dotenv()


class Settings(BaseSettings):
    """MQTT node settings."""

    client_id: Optional[Text]

    broker_host_url: Text

    # Default port number in MQTT for all unencrypted connections
    broker_port: PositiveInt = 1883

    storage_directory: Optional[Path]

    storage_size_limit_in_bytes: Optional[PositiveInt]

    single_client: Optional[mqtt.Client]

    publication_timeout: Optional[PositiveFloat]

    publication_time_interval: PositiveInt = 60

    outside: bool = False

    outside_name: Text = "outside"

    room_name: Text = "inside/room"

    room_sensors: List[Text] = []

    outside_sensors: List[Text] = ["weather"]

    subscriptions: List[Union[Text, List[Text]]] = []

    @validator(
        "room_sensors", "outside_sensors",
        pre=True, always=True
    )
    def parse_sensors(
            # pylint: disable=no-self-argument,no-self-use
            cls,
            sensors: Union[Text, List[Text]]
    ) -> List[Text]:
        """Parses list of sensors from text in JSON list syntax."""
        if not isinstance(sensors, List):
            sensors = srsly.json_loads(sensors)

        return sensors

    @property
    def client(self) -> mqtt.Client:
        """Property holding an MQTT client instance."""
        if not self.single_client:
            self.single_client = mqtt.Client(
                client_id=self.client_id
            )

            try:
                self.single_client.connect(
                    host=self.broker_host_url,
                    port=self.broker_port
                )
            except ConnectionRefusedError as connection_error:
                print(f"[red]{connection_error}[/]")

        return self.single_client

    @property
    def storage_size_in_bytes(self) -> PositiveInt:
        """The data storage size in bytes."""
        total_storage_size_in_bytes = 0

        if self.storage_directory and self.storage_directory.exists():
            for path in self.storage_directory.rglob("*"):
                if path.is_file():
                    total_storage_size_in_bytes += get_file_size(
                        path
                    )

        return total_storage_size_in_bytes

    @property
    def storage_within_limits(self) -> bool:
        """Whether the data storage is within in capacity limits."""
        if not self.storage_directory:
            return True

        if not self.storage_size_limit_in_bytes:
            return True

        return self.storage_size_in_bytes <= self.storage_size_limit_in_bytes

    @property
    def storage_enabled(self) -> bool:
        """Whether storage is enabled."""
        return bool(self.storage_directory)

    class Config(BaseConfig):
        """Pydantic config for the settings."""
        # pylint: disable=too-few-public-methods
        extra: Extra = Extra.allow


SETTINGS = Settings()
