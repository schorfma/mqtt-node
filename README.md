# 📡 MQTT Node <!-- omit in toc -->

> A distributed **_MQTT_ Node** written in _Python_
>
> * Publish sensor data to an _MQTT Broker_ (using a _Raspberry Pi_)
> * Persist subscribed _MQTT Topics_

### Included Sensor Implementations <!-- omit in toc -->

* [_BMP280_](#sensor-bmp280) (_Temperature_ and _Pressure_)
* [_DHT22_](#sensor-dht22) (_Temperature_ and _Humidity_)
* [_OpenWeather_ API](#sensor-weather) (Outside _Temperature_ and more weather values by [_OpenWeather API_](https://openweathermap.org/api))

### Table of Contents <!-- omit in toc -->

- [Installation](#installation)
    - [Installation in Virtual Environment using `poetry`](#installation-in-virtual-environment-using-poetry)
        - [Development Installation](#development-installation)
    - [Installing Extras for Sensors](#installing-extras-for-sensors)
        - [Installing Dependencies for _BMP280_ sensor](#installing-dependencies-for-bmp280-sensor)
        - [Installing Dependencies for _DHT22_ sensor](#installing-dependencies-for-dht22-sensor)
        - [Installing Dependencies for both Sensors](#installing-dependencies-for-both-sensors)
- [Configuration](#configuration)
    - [MQTT Broker / Client Settings](#mqtt-broker-client-settings)
    - [Storage-related Settings](#storage-related-settings)
    - [Publication Settings](#publication-settings)
    - [Subscription Settings](#subscription-settings)
- [Usage](#usage)
    - [<kbd>command</kbd> `mqtt-node run`](#command-mqtt-node-run)
    - [<kbd>command</kbd> `mqtt-node subscribe`](#command-mqtt-node-subscribe)
- [Registered Sensors](#registered-sensors)
    - [<kbd>sensor</kbd> `weather`](#sensor-weather)
        - [<kbd>class</kbd> `mqtt_node.sensors.open_weather_api.OpenWeatherAPISensor`](#class-mqtt_nodesensorsopen_weather_apiopenweatherapisensor)
    - [<kbd>sensor</kbd> `bmp280`](#sensor-bmp280)
        - [<kbd>class</kbd> `mqtt_node.sensors.bmp280_sensor.BMP280Sensor`](#class-mqtt_nodesensorsbmp280_sensorbmp280sensor)
    - [<kbd>sensor</kbd> `dht22`](#sensor-dht22)
        - [<kbd>class</kbd> `mqtt_node.sensors.dht22_sensor.DHT22Sensor`](#class-mqtt_nodesensorsdht22_sensordht22sensor)
    - [<kbd>sensor</kbd> `pressure`](#sensor-pressure)
    - [<kbd>sensor</kbd> `temperature`](#sensor-temperature)
    - [<kbd>sensor</kbd> `humidity`](#sensor-humidity)
- [Python API](#python-api)
    - [Validated Data Models](#validated-data-models)
        - [<kbd>class</kbd> `mqtt_node.model.Publication`](#class-mqtt_nodemodelpublication)
            - [<kbd>method</kbd> `publish() 🠂 Optional[Text]`](#method-publish-optionaltext)
        - [<kbd>class</kbd> `mqtt_node.model.Subscription`](#class-mqtt_nodemodelsubscription)
            - [<kbd>method</kbd> `subscribe() 🠂 Self`](#method-subscribe-self)
            - [<kbd>method</kbd> `message_callback(client, userdata, message) 🠂 None`](#method-message_callbackclient-userdata-message-none)

## Installation

`mqtt_node` requires _Python ≥ `3.9`_ and uses `poetry` for managing dependencies.

### Installation in Virtual Environment using `poetry`

Run the following command within the root directory containing the `pyproject.toml` file:

```sh
poetry install --no-dev
```

Afterwards the virtual environment can be activated using `poetry shell`.

Also commands may be executed in the virtual environment py prepending `poetry run` to a command (e.g. `poetry run mqtt-node run`)

#### Development Installation

```sh
poetry install
```

### Installing Extras for Sensors

The dependencies for accessing the sensors can be installed separately as extras.

#### Installing Dependencies for _BMP280_ sensor

```sh
# Using poetry
poetry install --no-dev --extras "bmp280"
```

#### Installing Dependencies for _DHT22_ sensor

```sh
# Using poetry
poetry install --no-dev --extras "dht22"
```

#### Installing Dependencies for both Sensors

```sh
# Using poetry
poetry install --no-dev --extras "all"
```

## Configuration

All configuration options may be set using a `.env` file or by setting environment variables.

### MQTT Broker / Client Settings

| **Setting**       | **Type**                                          | **Description**                                                                        |
| ----------------- | ------------------------------------------------- | -------------------------------------------------------------------------------------- |
| `client_id`       | `Text` (Optional)                                 | Name of the client device.                                                             |
| `broker_host_url` | `Text`                                            | IP address or name of the computer running the _MQTT Broker_ within the local network. |
| `broker_port`     | `Positive Integer` (Optional, Defaults to `1883`) | Port of the _MQTT Broker_.                                                             |

### Storage-related Settings

| **Setting**                   | **Type**                      | **Description**                                                              |
| ----------------------------- | ----------------------------- | ---------------------------------------------------------------------------- |
| `storage_directory`           | `Text` (Optional)             | Path to directory for persisting outgoing or incoming payloads.              |
| `storage_size_limit_in_bytes` | `Positive Integer` (Optional) | The maximum storage size of the stored data in bytes (Currently not in use). |

### Publication Settings

| **Setting**                 | **Type**                                                                 | **Description**                                                                                                                           |
| --------------------------- | ------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------- |
| `publication_timeout`       | `Positive Float` (Optional)                                              | The time in seconds until the publication of messages times out.                                                                          |
| `publication_time_interval` | `Positive Integer` (Optional, Defaults to `60` seconds)                  | The interval between message publications of sensors when using `mqtt-node run` command without a specified `--time-interval`.            |
| `outside`                   | `Boolean` (Optional, Defaults to `False`)                                | Whether the device location is outside.                                                                                                   |
| `outside_name`              | `Text` (Optional, Defaults to `"outside"`)                               | The _MQTT Topic_ hierarchy under which to send messages from outside sensors (`outside_sensors`) when using `mqtt-node run` command.      |
| `room_name`                 | `Text` (Optional, Defaults to `"inside/room"`)                           | The _MQTT Topic_ hierarchy under which to send messages from sensors inside the room (`room_sensors`) when using `mqtt-node run` command. |
| `room_sensors`              | _JSON_ list of strings as `Text` (Optional, Defaults to `'[]'`)          | A list of sensors on the device as registered in `mqtt_node.sensors` registry when using `mqtt-node run` command.                         |
| `outside_sensors`           | _JSON_ list of strings as `Text` (Optional, Defaults to `'["weather"]'`) | A list of sensors for the outside as registered in `mqtt_node.sensors` registry when using `mqtt-node run` command.                       |

### Subscription Settings

| **Setting**     | **Type**                                                        | **Description**                                                                     |
| --------------- | --------------------------------------------------------------- | ----------------------------------------------------------------------------------- |
| `subscriptions` | _JSON_ list of strings as `Text` (Optional, Defaults to `'[]'`) | List of _MQTT Topics_ to subscribe to when using the `mqtt-node subscribe` command. |

## Usage

After configuration _MQTT Node_ the provided services may be started using the following commands.

### <kbd>command</kbd> `mqtt-node run`

```sh
Usage: mqtt-node run [OPTIONS]

  Starts an MQTT Sensor node

Options:
  --time-interval INTEGER  [default: 60]  # Defaults to configured value.
  --help                   Show this message and exit.
```

### <kbd>command</kbd> `mqtt-node subscribe`

```sh
Usage: mqtt-node subscribe [OPTIONS]

  Subscribes to configured or specified MQTT topics.

Options:
  --topic TEXT  # Allows to specify additional subscription topics multiple times.
  --help        Show this message and exit.
```

## Registered Sensors

Several sensor wrappers are registered to be used as [`payload_function`](#class-mqtt_nodemodelpublication) for [`Publication`](#class-mqtt_nodemodelpublication) objects.

### <kbd>sensor</kbd> `weather`

The weather sensor provides current outside weather information for the specified location using the API of <https://openweathermap.org>.

| **Data Item**   | **Type**  | **Description**                       |
| --------------- | --------- | ------------------------------------- |
| `"location"`    | `Text`    | The location as a single string.      |
| `"latitude"`    | `Float`   | The latitude of the location.         |
| `"longitude"`   | `Float`   | The longitude of the location.        |
| `"weather"`     | `Text`    | A textual description of the weather. |
| `"temperature"` | `Float`   | The current outside temperature.      |
| `"pressure"`    | `Float`   | The current pressure in _hPa_.        |
| `"humidity"`    | `Float`   | The current humidity in _%_.          |
| `"visibility"`  | `Integer` | The current visibility distance.      |
| `"wind_speed"`  | `Float`   | The current wind speed.               |

#### <kbd>class</kbd> [`mqtt_node.sensors.open_weather_api.OpenWeatherAPISensor`](./mqtt_node/sensors/open_weather_api.py)

| **Attribute**            | **Type**                                                          | **Description**                                              |
| ------------------------ | ----------------------------------------------------------------- | ------------------------------------------------------------ |
| `open_weather_api_key`   | `Text`                                                            | The API key for <https://openweathermap.org/api>.            |
| `city_name`              | `Text` (Optional)                                                 | Name of the city or place to retrieve geo coordinates for.   |
| `state_code`             | `Text` (Optional)                                                 | Code of a US state, only applicable for locations in the US. |
| `country_code`           | `Text` (Optional)                                                 | ISO 3166 country code of the location, e.g. "DE"             |
| `latitude`               | `Float` (Optional)                                                | The latitude for retrieval of weather information.           |
| `longitude`              | `Float` (Optional)                                                | The longitude for retrieval of weather information.          |
| `open_weather_api_units` | `"standard"`, `"metric"` or `"imperial"` (Defaults to `"metric"`) | Which units system to use for the weather data.              |

Either the location information (`city_name`, `country_code`) or the geo coordinates (`latitude`, `longitude`) must be initially provided using environment variables or the `.env` file.

### <kbd>sensor</kbd> `bmp280`

The _BMP280_ sensor allows for measuring pressure in _hPa_ and temperature in _°C_.

| **Data Item**   | **Type** | **Description**                                 |
| --------------- | -------- | ----------------------------------------------- |
| `"temperature"` | `Float`  | The temperature measured by the sensor in _°C_. |
| `"pressure"`    | `Float`  | The pressure measured by the sensor in _hPa_.   |

#### <kbd>class</kbd> [`mqtt_node.sensors.bmp280_sensor.BMP280Sensor`](./mqtt_node/sensors/bmp280_sensor.py)

There are several optional settings which can be set using environment variables or the `.env` file.

Also the altitude could be calculated based on the pressure and temperature.

### <kbd>sensor</kbd> `dht22`

The _DHT22_ sensor allows for measuring humidity in _%_ and temperature in _°C_.

| **Data Item**   | **Type** | **Description**                                 |
| --------------- | -------- | ----------------------------------------------- |
| `"temperature"` | `Float`  | The temperature measured by the sensor in _°C_. |
| `"humidity"`    | `Float`  | The humidity measured by the sensor in _%_.     |

#### <kbd>class</kbd> [`mqtt_node.sensors.dht22_sensor.DHT22Sensor`](./mqtt_node/sensors/dht22_sensor.py)

There are no settings required. Per default the _GPIO 16_ pin is used.

### <kbd>sensor</kbd> `pressure`

The pressure sensor returns just the pressure measurement by the _BMP280_ sensor.

### <kbd>sensor</kbd> `temperature`

The temperature sensor returns the temperature as measured by the _DHT22_ sensor, if it is available.
Otherwise the measurement by the _BMP280_ sensor will be used, if available.

### <kbd>sensor</kbd> `humidity`

The humidity sensor returns just the humidity measurement by the _DHT22_ sensor.

## Python API

### Validated Data Models

#### <kbd>class</kbd> `mqtt_node.model.Publication`

| **Attribute**              | **Type**                                                                                      | **Description**                                                     |
| -------------------------- | --------------------------------------------------------------------------------------------- | ------------------------------------------------------------------- |
| `quality_of_service_level` | `mqtt_node.model.QualityOfServiceLevel` (Optional, Defaults to `QualityOfServiceLevel.QOS_0`) | The _MQTT_ quality of service (QOS) level (`0`, `1` or `2`).        |
| `topic`                    | `mqtt_node.model.Topic` (`Text` or `Tuple[Text, ...]` get converted to `Topic`)               | The _MQTT Topic_ to publish messages to.                            |
| `retain_last_message`      | `bool` (Optional, Defaults to `False`)                                                        | Whether the _MQTT Broker_ shall retain last messages.               |
| `payload_function`         | `Callable`                                                                                    | A function returning data as a scalar or `Dict` value, when called. |

```py
from datetime import datetime

from mqtt_node.model import Publication


# Creating a current datetime publication
publication = Publication(
    topic="my/topic",
    payload_function=datetime.now
)

# Directly calling publish()
payload = publication.publish()

# Calling the Publication object to publish
payload = publication()
```

##### <kbd>method</kbd> `publish() 🠂 Optional[Text]`

The `publish()` method publishes the payload retrieved using `payload_function`.

The payload also gets saved to a child directory of [`storage_directory`](#storage-related-settings) and returned by the method call.

This method also can be invoked by directly calling a `Publication` object.

#### <kbd>class</kbd> `mqtt_node.model.Subscription`

| **Attribute**              | **Type**                                                                                      | **Description**                                              |
| -------------------------- | --------------------------------------------------------------------------------------------- | ------------------------------------------------------------ |
| `quality_of_service_level` | `mqtt_node.model.QualityOfServiceLevel` (Optional, Defaults to `QualityOfServiceLevel.QOS_0`) | The _MQTT_ quality of service (QOS) level (`0`, `1` or `2`). |
| `topic`                    | `mqtt_node.model.Topic` (`Text` or `Tuple[Text, ...]` get converted to `Topic`)               | The _MQTT Topic_ to subscribe for messages to.               |

```py
import paho.mqtt.client as mqtt

from mqtt_node.model import Subscription

# Creating a subscription to a topic
subscription = Subscription(
    topic="my/topic
)

# Directly subscribing to the topic by calling subscribe()
subscription.subscribe()

# Subscribing by calling a Subscription object
subscription()

client: mqtt.Client = subscription.client

# Infinite loop waiting for subscribed messages
client.loop_forever()
```

##### <kbd>method</kbd> `subscribe() 🠂 Self`

The `subscribe()` method subscribes the _MQTT Client_ to the `topic`.
It can also be called by calling a `Subscription` object.

After subscription incoming messages will go through the method `message_callback()`.

##### <kbd>method</kbd> `message_callback(client, userdata, message) 🠂 None`

The `message_callback()` method gets invoked upon incoming messages for the subscribed `topic`.

In its standard implementation incoming message payloads get saved to a child directory of [`storage_directory`](#storage-related-settings).
This behavior could be overriden or extended in inheriting classes.
