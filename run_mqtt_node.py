"""Command for running sensor nodes publishing data regularly."""
import time
from datetime import datetime
from datetime import timedelta
from typing import Dict
from typing import List
from typing import Optional
from typing import Text

from catalogue import Registry
from rich import print  # pylint: disable=redefined-builtin
from rich.console import Console
from rich.pretty import Pretty
from rich.rule import Rule
from rich.syntax import Syntax
from rich.table import Table
from rich.tree import Tree
from typer import Typer

from mqtt_node.model import Publication
from mqtt_node.model.subscription import Subscription
from mqtt_node.model.topic import Topic
from mqtt_node.sensors import SENSORS
from mqtt_node.settings import SETTINGS


MQTT_NODE_COMMAND = Typer()
RICH_CONSOLE = Console()


def initialize_sensor_publications(
        topic: Text,
        sensors: List[Text],
        registry: Registry = SENSORS
) -> Dict[Text, Publication]:
    """Initializes sensor publication based on the sensor registry."""
    return {
        sensor_key: Publication(
            topic=(topic, sensor_key),
            payload_function=registry.get(sensor_key)
        )
        for sensor_key in sensors
    }


def topic_tree(topic: Topic) -> Tree:
    """Renders a rich tree from a topic."""
    topic_parts = list(topic.topic)

    topic_tree_root = None
    current_topic_node = None

    while topic_parts:
        topic_part = topic_parts.pop(0)

        topic_node = Tree(topic_part)

        if not topic_tree_root:
            topic_tree_root = topic_node
        else:
            current_topic_node.add(topic_node)

        current_topic_node = topic_node

    return topic_tree_root


@MQTT_NODE_COMMAND.command("run")
def run_mqtt_node(
        time_interval: int = SETTINGS.publication_time_interval
):
    """Starts an MQTT Sensor node"""
    assert time_interval > 0

    room_name = SETTINGS.room_name
    room_sensors = initialize_sensor_publications(
        topic=room_name,
        sensors=SETTINGS.room_sensors
    )

    outside_name = SETTINGS.outside_name
    outside_sensors = initialize_sensor_publications(
        topic=outside_name,
        sensors=SETTINGS.outside_sensors
    )

    while True:
        print(
            Rule(str(datetime.now()))
        )

        message_table = Table(
            "Sensor Name", "Topic", "Payload",
            title=str(datetime.now())
        )

        for room_sensor_name, room_sensor in room_sensors.items():
            payload = room_sensor()
            message_table.add_row(
                room_sensor_name,
                topic_tree(room_sensor.topic),
                Syntax(
                    payload,
                    lexer="json",
                    indent_guides=True
                ) if isinstance(payload, Text) else Pretty(
                    payload
                ),
                end_section=True
            )

        for outside_sensor_name, outside_sensor in outside_sensors.items():
            payload = outside_sensor()
            message_table.add_row(
                outside_sensor_name,
                topic_tree(outside_sensor.topic),
                Syntax(
                    payload,
                    lexer="json",
                    indent_guides=True
                ) if isinstance(payload, Text) else Pretty(
                    payload
                ),
                end_section=True
            )

        print()
        print(message_table)
        print()

        now = datetime.now()

        seconds_to_wait = (
            timedelta(
                seconds=time_interval
            ) - timedelta(
                hours=now.hour,
                minutes=now.minute,
                seconds=now.second,
                microseconds=now.microsecond
            )
        ).total_seconds() % time_interval

        print(now, f"(+ [cyan]{seconds_to_wait:.2f} seconds[/])")
        with RICH_CONSOLE.status(
                f"Waiting [cyan bold]{seconds_to_wait:.2f}[/] [cyan]seconds[/] ..."
        ):
            time.sleep(seconds_to_wait)


@MQTT_NODE_COMMAND.command("subscribe")
def mqtt_node_subscriptions(topic: Optional[List[Text]] = None):
    """Subscribes to configured or specified MQTT topics."""

    subscription_topics = SETTINGS.subscriptions
    if topic:
        subscription_topics.extend(topic)

    # pylint: disable=unused-variable
    subscriptions: Dict[Text, Subscription] = {
        subscription_topic: Subscription(
            topic=subscription_topic
        ).subscribe()
        for subscription_topic in subscription_topics
    }

    print("Subscribed to")
    print(subscription_topics)

    SETTINGS.client.loop_forever()


if __name__ == "__main__":
    MQTT_NODE_COMMAND()
